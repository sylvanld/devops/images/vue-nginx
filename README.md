# 

Custom nginx image that substitute production environment variables in your vuejs built app.

## How does it works ?

In a Vue.js app, you can use environment variables in your js/ts code by using `process.env.VUE_APP_MY_VARIABLE`.
When building for production, all `process.env.VUE_APP*` are substituted by values hardcoded in .env.production.
But with this process you need to rebuild your image with a different `.env.production` for each environment.

Instead of this, this docker image assume that your `.env.production` looks like the following. (i.e where value of variables are variable name itself).

```
VUE_APP_MY_VARIABLE_1=VUE_APP_MY_VARIABLE_1
VUE_APP_ANOTHER_VARIABLE=VUE_APP_ANOTHER_VARIABLE
```

This way, vue-nginx image is able to find these variables in your built app, and substitute it by actual environment variables of the container when it is ran.

## Usage

- In your vuejs app [use environment variables](https://cli.vuejs.org/guide/mode-and-env.html) to define custom settings.

- Before building your vue app, ensure `.env.production` values respect the pattern `VUE_APP_<VARIABLE_NAME>=VUE_APP_<VARIABLE_NAME>`

> Be carefull as the `VUE_APP_` prefix is mandatory.

*example given*

```
VUE_APP_MY_VARIABLE_1=VUE_APP_MY_VARIABLE_1
VUE_APP_ANOTHER_VARIABLE=VUE_APP_ANOTHER_VARIABLE
```

- Build your vuejs app and name builder image so that it can be reused in multi-step build.

```
FROM node:lts-alpine AS node-builder
WORKDIR /build
...
npm run build
```

- Copy built files from builder image to this image

```dockerfile
FROM registry.gitlab.com/sylvanld/devops/images/vue-nginx:<TAG>
COPY --from=node-builder /build/dist /usr/share/nginx/template
```

```
docker build -t my-image:v1 .
```

- Done ! You can now run your image

```
docker run -e VUE_APP_ANOTHER_VARIABLE=hello my-image:v1
```

With the previous example, this should output :

```
Checking environment variables
- Variable "VUE_APP_MY_VARIABLE_1" is missing !
- Variable "VUE_APP_ANOTHER_VARIABLE" defined with value "hello"

Missing environment variable, abort startup...
```
