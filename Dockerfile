FROM nginx:$NGINX_IMAGE_TAG

COPY nginx/nginx.conf /etc/nginx/nginx.conf
COPY nginx/default.conf /etc/nginx/conf.d/default.conf
COPY bootstrap.sh /etc/nginx/bootstrap.sh

CMD ["/etc/nginx/bootstrap.sh"]
