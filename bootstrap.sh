TEMPLATE_ROOT=/usr/share/nginx/template
WEB_ROOT=/usr/share/nginx/html

# copy template to web root
rm -rf $WEB_ROOT
cp -r $TEMPLATE_ROOT $WEB_ROOT

# find app script in which variables must be substituted
APP_JS_PATH=$(ls $WEB_ROOT/js/app.*.js$)
REQUIRED_VARIABLES=$(grep -oP 'VUE_APP_\K[A-Z0-9_]+' $APP_JS_PATH)

# Substitute environment variables from template in running app
echo "Checking environment variables..."

for variable in $REQUIRED_VARIABLES
do
    value=$(eval echo \$$variable)
    if [ -z $value ]
    then
        echo "- Variable '$variable' is missing !"
        variable_missing=1
    else
        echo "- Variable '$variable' defined with value '$value'"
        sed -i "s|VUE_APP_$variable|$value|g" $APP_JS_PATH
    fi
done

# Abort execution if an environment variable is missing
if [ -n "$variable_missing" ]
then
    echo && echo "Missing environment variable, abort startup..."
    exit 1
fi

# Start nginx
echo && echo "Starting nginx server..."
nginx -g "daemon off;"
